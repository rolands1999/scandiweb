<!DOCTYPE html>
<html lang="en">
<?php
	require 'connect.php';
?>
<head>
	<title>
		Product list
	</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style type="text/css">
		body{
			margin: 0;
			font-family: 'Roboto', sans-serif;
			background-color: #E8E8E8;
		}
		#header{
			height: 60px;
			width: 100%;
			background-color: #e04e4f;
			box-shadow: 0 3px 3px #B0B0B0;
			position: fixed;
			z-index: 2;
		}
		#title{
			color: white;
			position: absolute;
			font-size: 30px;
			padding: 0;
			margin: 0;
			top: 10px;
			left: 10px;
		}
		#container{
			width: 1200px;
			margin: auto;
			position: relative;
			top: 60px;
		}
		#item{
			width: 250px;
			height: 300px;
			border: 1px solid #CFCFCF;
			display: inline-block;
			margin: 22px;
			text-align: center;
			background-color: white;
			position: relative;
		}
		#sku{
			text-align: left;
			margin-left: 10px;
			font-size: 18px;
			color: #828282; 
		}
		#name{
			font-size: 20px;
			font-weight: bold;
			margin-top: 40px;
		}
		#price{
			text-align: right;
			margin-right: 10px;
			font-size: 18px;
			margin-top: 60px;
		}
		#detail{
			text-align: left;
			margin-top: 50px;
			margin-left: 10px;
		}
		#checkbox{
			position: absolute;
			top: 0;
			right: 0;
			width: 15px;
			height: 15px;
		}
		#delete{
			float: right;
			width: 70px;
			height: 30px;
			position: relative;
			top: 14px;
			right: 10px;
			border: none;
			background-color: white;
			font-size: 14px;
			color: #e04e4f;
			border-radius: 3px;
			font-weight: bold;
			outline: 0;
		}
		#delete:hover{
			cursor: pointer;
			background-color: #9E9E9E;
			color: white;
		}
	</style>
</head>
<body>
	<?php
		$get_dvd = "SELECT * FROM `dvd`";
		$result_dvd = mysqli_query($con, $get_dvd);
		$get_books = "SELECT * FROM `books`";
		$result_books = mysqli_query($con, $get_books);
		$get_furn = "SELECT * FROM `furniture`";
		$result_furn = mysqli_query($con, $get_furn);
	?>
	<script>
		var items = [];
		function del(){
			$(document).ready(function(){
				$("input:checked").each(function(){
					var value = $(this).siblings("#sku").text();
					items.push(value);
					$(this).prop("checked", false);
				});
				$(items).each(function(){
					var xhttp = new XMLHttpRequest();
					xhttp.open("GET", "delete.php?sku=" + this, true);
					xhttp.send();
				});
				setTimeout(function(){
					$("body").load("list.php");
				}, 100);
			});
		}
	</script>
	<div id="header">
		<p id="title">PRODUCT LIST</p>
		<input type="button" value="Delete" id="delete" onClick=del();> 
	</div>
	<div id="container">
		<?php
			while($row_dvd = mysqli_fetch_assoc($result_dvd)){
				echo '<div id="item">
						<input type="checkbox" name="check" id="checkbox">
						<p id="sku">'.$row_dvd['sku'].'</p>
						<p id="name">'.$row_dvd['name'].'</p>
						<p id="price">'.$row_dvd['price'].' $</p>
						<p id="detail">Size: '.$row_dvd['size'].' MB</p>
					</div>';
			}
			while($row_books = mysqli_fetch_assoc($result_books)){
				echo '<div id="item">
						<input type="checkbox" name="check" id="checkbox">
						<p id="sku">'.$row_books['sku'].'</p>
						<p id="name">'.$row_books['name'].'</p>
						<p id="price">'.$row_books['price'].' $</p>
						<p id="detail">Weight: '.$row_books['weight'].' kg</p>
					</div>';
			}
			while($row_furn = mysqli_fetch_assoc($result_furn)){
				echo '<div id="item">
						<input type="checkbox" name="check" id="checkbox">
						<p id="sku">'.$row_furn['sku'].'</p>
						<p id="name">'.$row_furn['name'].'</p>
						<p id="price">'.$row_furn['price'].' $</p>
						<p id="detail">Dimensions: '.$row_furn['height'].' x '.$row_furn['width'].' x '.$row_furn['length'].'</p>
					</div>';
			}
		?>
	</div>
	
</body>
</html>