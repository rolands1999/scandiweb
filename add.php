<!DOCTYPE html>
<html lang="en">
<?php
	require 'insert.php';
?>
<head>
	<title>
		Add product
	</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style type="text/css">
		body{
			margin: 0;
			font-family: 'Roboto', sans-serif;
			background-color: #E8E8E8;
		}
		#header{
			height: 60px;
			width: 100%;
			background-color: #e04e4f;
			box-shadow: 0 3px 3px #B0B0B0;
			position: fixed;
			z-index: 2;
		}
		#title{
			color: white;
			position: absolute;
			font-size: 30px;
			padding: 0;
			margin: 0;
			top: 10px;
			left: 10px;
		}
		#add{
			float: right;
			width: 70px;
			height: 30px;
			position: relative;
			top: 14px;
			right: 10px;
			border: none;
			background-color: white;
			font-size: 14px;
			color: #e04e4f;
			border-radius: 3px;
			font-weight: bold;
			outline: 0;
		}
		#add:hover{
			cursor: pointer;
			background-color: #9E9E9E;
			color: white;
		}
		#container{
			width: 1200px;
			margin: auto;
			position: relative;
			top: 60px;
		}
		#field{
			width: 250px;
			height: 30px;
			display: block;
			margin: auto;
			position: relative;
			top: 60px;
			margin-bottom: 20px;
			font-size: 16px;
			outline: none;
		}
		#product{
			width: 250px;
			height: 300px;
			border: 1px solid #CFCFCF;
			text-align: center;
			background-color: white;
			position: relative;
			margin: auto;
			top: 100px;
		}
		#sku{
			text-align: left;
			margin-left: 10px;
			font-size: 18px;
			color: #828282; 
		}
		#name{
			font-size: 20px;
			font-weight: bold;
			margin-top: 40px;
		}
		#price{
			text-align: right;
			margin-right: 10px;
			font-size: 18px;
			margin-top: 60px;
		}
		#detail{
			text-align: left;
			margin-top: 50px;
			margin-left: 10px;
		}
		#checkbox{
			position: absolute;
			top: 0;
			right: 0;
			width: 15px;
			height: 15px;
		}
		#info{
			width: 250px;
			height: 70px;
			border: 1px solid #9E9E9E;
			margin: auto;
			position: relative;
			top: 60px;
			color: #757575;
			text-align: center;
		}
	</style>
</head>
<body>
	<script>
		var type;
		$(document).ready(function(){
			$(".detail").hide();
			$(".detail2").hide();
			$(".detail3").hide();
			$("#info").hide();
			$(".sku").keyup(function(){
				if($(this).val() == ""){
					$("#sku").text("SKU");
				}else{
					$("#sku").text($(this).val());
				}
			});
			$(".name").keyup(function(){
				if($(this).val() == ""){
					$("#name").text("Product name");
				}else{
					$("#name").text($(this).val());
				}
			});
			$(".price").keyup(function(){
				if($(this).val() == ""){
					$("#price").text("Price");
				}else{
					var cur = parseFloat($(this).val());
					$("#price").text(cur.toFixed(2) + " $");
				}
			});
			$(".select").change(function(){
				type = $(this).val();
				if(type == "1"){
					$(".detail").attr("placeholder", "Size");
					$("#info").children().text("Specify the size of a DVD-disc in MB");
					$(".detail").show();
					$("#info").show();
					$(".detail2").hide();
					$(".detail3").hide();
				}else if(type == "2"){
					$(".detail").attr("placeholder", "Weight");
					$("#info").children().text("Specify the weight of the book in kg");
					$(".detail").show();
					$("#info").show();
					$(".detail2").hide();
					$(".detail3").hide();
				}else{
					$(".detail").attr("placeholder", "Height");
					$(".detail2").attr("placeholder", "Width");
					$(".detail3").attr("placeholder", "Length");
					$(".detail2").attr("disabled", true);
					$(".detail3").attr("disabled", true);
					$("#info").children().text("Specify dimensions in cm");
					$(".detail").show();
					$(".detail2").show();
					$(".detail3").show();
					$("#info").show();
				}
			});
			$(".detail").keyup(function(){
				if(type == "1"){
					if($(this).val() == ""){
						$("#detail").text("Details");
					}else{
						$("#detail").text("Size: " + $(this).val() + " MB");
					}
				}else if(type == "2"){
					if($(this).val() == ""){
						$("#detail").text("Details");
					}else{
						$("#detail").text("Weight: " + $(this).val() + " kg");
					}
				}else{
					if($(this).val() == ""){
						$("#detail").text("Details");
						$(".detail2").val("");
						$(".detail2").attr("disabled", true);
						$(".detail3").val("");
						$(".detail3").attr("disabled", true);
					}else{
						$("#detail").text("Dimensions: " + $(this).val() + " cm");
						$(".detail2").attr("disabled", false);
					}
				}
			});
			$(".detail2").keyup(function(){
				var prev = $(".detail").val();
				if($(this).val() == ""){
					$("#detail").text("Dimensions: " + prev + " cm");
					$(".detail3").val("");
					$(".detail3").attr("disabled", true);
				}else{
					$("#detail").text("Dimensions: " + prev + " x " + $(this).val() + " cm");
					$(".detail3").attr("disabled", false);
				}
			});
			$(".detail3").keyup(function(){
				var prev2 = $(".detail").val() + " x " + $(".detail2").val();
				if($(this).val() == ""){
					$("#detail").text("Dimensions: " + prev2 + " cm");
				}else{
					$("#detail").text("Dimensions: " + prev2 + " x " + $(this).val() + " cm");
				}
			});
			$("#add").click(function(){
				var sku = $(".sku").val();
				var name = $(".name").val();
				var price = $(".price").val();
				var type = $(".select").val();
				var detail = $(".detail").val();
				if(sku != "" && name != "" && price != "" && type != "" && detail != ""){
					if(type == "1" || type == "2"){
						$("#product_form").submit();
					}else{
						var detail2 = $(".detail2").val();
						var detail3 = $(".detail3").val();
						if(detail2 != "" && detail3 != ""){
							$("#product_form").submit();
						}else{
							alert("Please make sure none of the fields are empty!");
						}
					}
				}else{
					alert("Please make sure none of the fields are empty!");
				}
			});
		});
	</script>
	<div id="header">
		<p id="title">ADD PRODUCT</p>
		<input type="button" value="Add" id="add">
	</div>
	<div id="container">
		<form id="product_form" action="add.php" method="POST">
			<input type="text" name="sku" id="field" class="sku" placeholder="SKU">
			<input type="text" name="name" id="field" class="name" placeholder="Name">
			<input type="number" name="price" id="field" step="0.01" min="0" class="price" placeholder="Price">
			<select name="type" id="field" class="select">
				<option disabled selected hidden>Product type</option>
				<option value="1">DVD-disc</option>
				<option value="2">Book</option>
				<option value="3">Furniture</option>
			</select>
			<input type="number" id="field" name="detail" class="detail" step="1" min="0" placeholder="">
			<input type="number" id="field" name="detail2" class="detail2" step="1" min="0" placeholder="">
			<input type="number" id="field" name="detail3" class="detail3" step="1" min="0" placeholder="">
			<div id="info">
				<p></p>
			</div>
		</form>
		<div id="product">
			<input type="checkbox" name="check" id="checkbox">
			<p id="sku">SKU</p>
			<p id="name">Product name</p>
			<p id="price">Price</p>
			<p id="detail">Details</p>
		</div>
		<div>
	</div>
</body>
</html>